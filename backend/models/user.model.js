const mongoose = require("mongoose");
require("mongoose-type-email");

const Schema = mongoose.Schema;

const ContactSchema = new Schema({
  name: { type: String, required: true },
  telephone: [Number],
  email: [{ type: mongoose.SchemaTypes.Email }],
  address: { type: String },
  favorite: Boolean,
  birthday: Date,
});

const UserSchema = new Schema(
  {
    email: {
      type: mongoose.SchemaTypes.Email,
      required: "Email is required",
      unique: true,
    },
    username: {
      type: String,
      required: "Unique username is required",
      trim: true,
    },
    password: { type: String, required: "password is required", minlength: 6 },
    contactList: [{ type: ContactSchema, required: false }],
  },
  { timestamps: true }
);

const User = mongoose.model("User", UserSchema);

module.exports = User;
