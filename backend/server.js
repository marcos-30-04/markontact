const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const passport = require("passport");
const session = require("express-session");
const cookieParser = require("cookie-parser");

require("dotenv").config();

// Passport config
require("./config/passport")(passport);

const app = express();
const port = process.env.PORT || 5000;

app.use(express.json());

// Bodyparser
app.use(express.urlencoded({ extended: true }));

app.use(
  cors({
    origin: "https://markontact.netlify.app", // <-- location of the react app were connecting to
    credentials: true,
  })
);

// Connect to MongoDB
const uri = process.env.ATLAS_URI;
mongoose.connect(uri, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});
const connection = mongoose.connection;
connection.once("open", () => {
  console.log("MongoDB database connection established succesfully");
});

// Express Session
app.use(
  session({
    secret: "ecuador2023",
    resave: true,
    saveUninitialized: true,
  })
);

// Cookie-parser
app.use(cookieParser("ecuador2023"));

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

// Routes
const contactsRouter = require("./routes/contacts");
const usersRouter = require("./routes/users");

app.use("/users", contactsRouter);
app.use("/users", usersRouter);
app.get("/", (req, res) => {
  res.send("Hello from Express!");
});

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});
