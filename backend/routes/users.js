const router = require("express").Router();

const {
  loggedUser,
  newUser,
  login,
  logout,
} = require("../controllers/userControllers");

router.route("/user").get(loggedUser);

router.route("/register").post(newUser);

router.post("/login", login);

router.get("/logout", logout);

module.exports = router;
