const router = require("express").Router();
const { ensureAuthenticated } = require("../config/auth");
const {
  allContacts,
  addUser,
  oneContact,
} = require("../controllers/contactsControllers");

router.get("/:user/contactlist", ensureAuthenticated, allContacts);

router.route("/:user/contactlist/add").post(addUser);

router.route("/:user/contactlist/:id").get(oneContact);

module.exports = router;
