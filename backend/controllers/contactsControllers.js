const User = require("../models/user.model");

// GET '/:user/contactlist'
const allContacts = (req, res) => {
  User.findOne({ username: `${req.params.user}` })
    .then((user) => {
      res.json(user.contactList);
    })
    .catch((err) => res.status(400).json("Error: " + err));
};

// POST '/:user/contactlist/add'

const addUser = (req, res) => {
  const name = req.body.name;
  const telephone = Number(req.body.telephone) || [];
  const email = req.body.email || [];
  const address = req.body.address || "";
  const favorite = req.body.favorite || false;
  const birthday = Date(req.body.birthday) || "";

  const newContact = {
    name: name,
    telephone: telephone,
    email: email,
    address: address,
    favorite: favorite,
    birthday: birthday,
  };
  User.findOneAndUpdate(
    { username: `${req.params.user}` },
    { $push: { contactList: newContact } }
  )
    .then(() => res.json("Contact added!"))
    .catch((err) => res.status(400).json("Error: " + err));
};

const oneContact = (req, res) => {
  User.findOne({ username: `${req.params.user}` })
    .then((user) => {
      const contacts = user.contactList;
      const contact = contacts.find(
        (individual) => individual["_id"] === req.params.id
      );
      res.json(contact);
    })
    .catch((err) => res.status(400).json("Error: " + err));
};

// DELETE '/:user/contactlist/add'

// hacer una query para eliminar un sólo contacto
// hay que establecer el usuario y encontrar el contacto por id

// router.route("/:user/contactlist/:id").delete((req, res) => {
//   const filter = { username: `${req.params.user}` };
//   const update = () => {
//     contactList = this.contactList.filter(
//       (contact) => contact["_id"] == req.params.id
//     );
//   };
//   User.findOneAndUpdate(filter, update)
//     .then((user) => res.json(user))
//     .catch((err) => res.status(400).json("Error: " + err));
// });

// hacer una query para actualizar un contacto
// hay que establecer el usuario y encontrar el contacto por id
// tener en cuenta que se pueden agregar tel, email, y todas las opcionales, como tambien modificarlas y/o eliminarlas

// router.route('/:user/contactlist/update/:id').post((req, res) => {

// })

module.exports = { allContacts, addUser, oneContact };
