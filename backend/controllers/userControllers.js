const User = require("../models/user.model");
const bcrypt = require("bcryptjs");
const passport = require("passport");

// GET '/user'

const loggedUser = (req, res) => {
  console.log(req.user);
  const user = req.user;
  res.send(user);
};

// POST '/register

const newUser = (req, res) => {
  const { email, username, password, password2 } = req.body;
  const contactList = [];
  const errors = [];

  // Check required fields
  if (!email || !username || !password || !password2) {
    errors.push({ msg: "please fill in all fields" });
  }

  // Check password match
  if (password !== password2) {
    errors.push({ msg: "Passwords do not match" });
  }

  // Check passwords length
  if (password.length < 6) {
    errors.push({ msg: "Password should be at least 6 characters" });
  }

  // Can't Validate
  if (errors.length > 0) {
    return res.send(errors);
  } else {
    // Validation Passed
    User.findOne({ email: email })
      .then((user) => {
        if (user) {
          // User Exists
          errors.push({ msg: "Email is already registered" });
          return res.send(errors);
        } else {
          const newUser = new User({
            email,
            username,
            password,
            contactList,
          });
          // Hash Password
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, (err, hashedPassword) => {
              if (err) throw err;
              // Set password to hashed
              newUser.password = hashedPassword;
              // Save user
              newUser
                .save()
                .then(() =>
                  res.send([{ msg: "You are registered. You can login now!" }])
                )
                .catch((err) => res.status(400).json("Error: " + err));
            });
          });
        }
      })
      .catch((err) => res.status(400).json("Error: " + err));
  }
};

// POST '/login'
const login = (req, res, next) => {
  passport.authenticate("local", (err, user, info) => {
    if (err) {
      return next(err);
    }
    // Redirect if it fails
    if (!user) {
      return res.send([{ msg: `${info.message}` }]);
    }

    req.logIn(user, (err) => {
      if (err) return next(err);

      // Redirect if it succeeds
      return res.send([
        {
          msg: "You are now logged in",
          user: `${user.username}`,
        },
      ]);
    });
  })(req, res, next);
};

// GET '/logout'
const logout = (req, res) => {
  req.logout();
  // req.flash("success_msg", "You are logged out");
  res.send({ msg: "You Are now logged out" });
};

module.exports = {
  loggedUser,
  newUser,
  login,
  logout,
};
