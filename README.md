### Markontact

# This is a, like the name implies, contacts app made by me, Marcos.

## The App is still a work in progress.

The main functionality however is done. the main issues that i'm facing right now, have to do with the rendering order of the components and the inherent problems that it causes to the user session. 

i'm also trying to fix the, perplexing to me, problem that it has rendering in chrome, as it seems to not be quite the same as in firefox.

The backend has given me no problem so far, but some functionalities are to be created for a more complete app.


Much work is to be done before i can call it 'finished'

### Technologies

The Front-End is **React**, the user session is to be handled by context, so the state is available to the whole app without dealing with a messy inheritance in the components. The request is handled by **axios**.
The Front-end is freely hosted on Netlify, so thanks to them.

The Back-End is **Node.Js** using **Express.js**. It's connected to a **MongoDB Atlas** database, so it's up and running on the cloud, and the model schemas are made using **Mongoose**. Authentication is session based, and managed with **Passport**. 
The Back-End is hosted freely on Heroku, so thanks to them too.



