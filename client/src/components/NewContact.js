import React, { useState } from "react";
import {
  Container,
  Form,
  Col,
  Badge,
  Button,
  ButtonGroup,
  Alert,
} from "react-bootstrap";
import "./../App.css";
import axios from "axios";
import { useGlobalContext } from "../context";

const NewContact = (props) => {
  const { user } = useGlobalContext();

  const [person, setPerson] = useState({
    name: "",
    address: "",
    telephone: "",
    email: "",
    favorite: false,
  });

  const variant = person.favorite ? "warning" : "outline-warning";

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setPerson({ ...person, [name]: value });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (!person.name) {
      return <Alert severity="error">ponenombre</Alert>;
    }

    const contact = {
      name: person.name,
      address: person.address,
      telephone: person.telephone,
      email: person.email,
      favorite: person.favorite,
    };

    axios
      .post(
        `https://markontact.herokuapp.com/users/${user}/contactlist/add`,
        contact
      )
      .then((res) => console.log(res.data));
  };

  return (
    <Container className="NewContact">
      <Badge variant="secondary">New Contact</Badge>
      <Form onSubmit={onSubmit}>
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>Name*</Form.Label>
            <Form.Control
              value={person.name}
              name="name"
              onChange={handleChange}
              type="text"
              placeholder="Name"
            />
          </Form.Group>

          <Form.Group as={Col}>
            <Form.Label>Address</Form.Label>
            <Form.Control
              value={person.address}
              type="text"
              name="address"
              placeholder="Address"
              onChange={handleChange}
            />
          </Form.Group>

          <Form.Group as={Col}>
            <Form.Label>Phone Number</Form.Label>
            <Form.Control
              value={person.telephone}
              type="number"
              name="telephone"
              placeholder="Telephone"
              onChange={handleChange}
            />
          </Form.Group>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>Email </Form.Label>
            <Form.Control
              value={person.email}
              type="email"
              name="email"
              placeholder="Enter email"
              onChange={handleChange}
            />
          </Form.Group>
          <ButtonGroup
            style={{ display: "flex", justifyContent: "space-around" }}
          >
            <Col xs={2}>
              <Button
                value={person.favorite}
                name="favorite"
                variant={variant}
                onClick={handleChange}
              >
                Favorite
              </Button>
            </Col>
            <Col xs={3}>
              <Button type="submit" variant="success">
                Add{" "}
              </Button>
            </Col>

            <Col xs={3}>
              <Button variant="danger">Clear</Button>
            </Col>
          </ButtonGroup>
        </Form.Row>
      </Form>
    </Container>
  );
};

export default NewContact;
