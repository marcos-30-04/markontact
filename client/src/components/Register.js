import axios from "axios";
import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import {
  Container,
  Form,
  Col,
  Badge,
  Button,
  ButtonGroup,
} from "react-bootstrap";

const Register = () => {
  const [isRegistered, setIsRegistered] = useState(false);
  const [newUser, setNewUser] = useState({
    email: "",
    username: "",
    password: "",
    password2: "",
    contactList: [],
  });
  const [alerts, setAlerts] = useState();

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setNewUser({ ...newUser, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .post("https://markontact.herokuapp.com/users/register", newUser)
      .then((res) => {
        setAlerts(res.data);
      });
  };
  useEffect(() => {
    if (alerts) {
      if (alerts[0].msg === "You are registered. You can login now!") {
        setTimeout(() => setIsRegistered(true), 3000);
      }
    }
  }, [alerts]);

  return (
    <>
      {isRegistered ? (
        <Redirect to="/login" />
      ) : (
        <Container className="NewUser" fluid="sm" style={{ maxWidth: 400 }}>
          <Badge variant="secondary">Register</Badge>
          {alerts &&
            alerts.map((alert) => {
              return (
                <h6
                  className={
                    alert.msg === "You are registered. You can login now!"
                      ? "alert-msg success"
                      : "alert-msg danger"
                  }
                  key={alert.msg}
                >
                  {alert.msg}
                </h6>
              );
            })}
          <Form onSubmit={handleSubmit}>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                value={newUser.email}
                name="email"
                onChange={handleChange}
                type="text"
                placeholder="Email"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control
                value={newUser.username}
                type="text"
                name="username"
                placeholder="Username"
                onChange={handleChange}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control
                value={newUser.password}
                type="password"
                name="password"
                placeholder="Password"
                onChange={handleChange}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Confirm Password </Form.Label>
              <Form.Control
                value={newUser.password2}
                type="password"
                name="password2"
                placeholder="Confirm Password"
                onChange={handleChange}
              />
            </Form.Group>
            <ButtonGroup
              style={{
                display: "flex",
                justifyContent: "right",
                paddingBottom: 10,
              }}
            >
              <Col>
                <Button type="submit" variant="secondary">
                  Register{" "}
                </Button>
              </Col>
            </ButtonGroup>
          </Form>
        </Container>
      )}
    </>
  );
};

export default Register;
