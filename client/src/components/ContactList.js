import React, { useState, useEffect } from "react";
import axios from "axios";
import { Container } from "react-bootstrap";
import Contact from "./Contact";
import { useGlobalContext } from "../context";

const ContactList = () => {
  const { user, setLoading } = useGlobalContext();
  const [contacts, setContacts] = useState([]);

  useEffect(() => {
    setLoading(true);
    const getContacts = () => {
      axios
        .get(`https://markontact.herokuapp.com/users/${user}/contactlist`)
        .then((response) => {
          setContacts(response.data);
        })
        .finally(setLoading(false))
        .catch((error) => {
          console.log(error);
        });
    };
    getContacts();
  }, [user, setLoading]);

  const deleteContact = (id) => {
    // axios to delete contact of certain user
    axios.delete();
    // maybe specify user through parameters
    console.log(id);
  };

  const list = () =>
    contacts.map((contact) => {
      return (
        <Contact
          contact={contact}
          key={contact._id}
          deleteContact={deleteContact}
        />
      );
    });

  return (
    <React.Fragment>
      <h3 className="pageTitle">contacts</h3>
      <Container className="list-container">{list()}</Container>
    </React.Fragment>
  );
};

export default ContactList;
