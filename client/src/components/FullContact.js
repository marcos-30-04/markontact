import React, { useEffect, useState } from "react";
import { Container, Spinner } from "react-bootstrap";
import { useGlobalContext } from "../context";

const FullContact = ({ match }) => {
  const { fullContact, getFullContact, setLoading } = useGlobalContext();
  const userId = match.params.id;

  const [contact, setContact] = useState();

  const displayNumbers = (arr) => {
    arr.map((number) => {
      return (
        <div key={number} className="telephone-numbers">
          {number}
        </div>
      );
    });
  };

  useEffect(() => {
    setLoading(true);
  }, []);

  useEffect(() => {
    const contactData = async () => {
      const data = await getFullContact(userId);
      setContact(data);
    };
    contactData();
  }, [fullContact]);

  console.log(contact);

  return (
    <>
      {contact ? (
        <Container
          className={fullContact.favorite ? "Contact favorite" : "Contact"}
        >
          <article className="contact-fields">
            <h2>Name: {fullContact.name}</h2>
            <h2>Email: {fullContact.email}</h2>
            <h2>Telephones: {displayNumbers(fullContact.telephone)}</h2>
            <h2>Address: {fullContact.address}</h2>
          </article>
        </Container>
      ) : (
        <div
          className="loading"
          style={{ color: "white", margin: 100, fontSize: "50px" }}
        >
          <h1>Loading...</h1>
          <Spinner animation="border" variant="light" />
        </div>
      )}
    </>
  );
};

export default FullContact;
