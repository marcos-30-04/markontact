import axios from "axios";
import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import {
  Container,
  Form,
  Col,
  Badge,
  Button,
  ButtonGroup,
} from "react-bootstrap";

const Login = () => {
  const [loginInfo, setLoginInfo] = useState({ email: "", password: "" });
  const [response, setResponse] = useState();

  const [isLogged, setIsLogged] = useState(false);

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setLoginInfo({ ...loginInfo, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post("https://markontact.herokuapp.com/users/login", loginInfo)
      .then((res) => {
        setResponse(res.data);
      });
  };

  useEffect(() => {
    if (response) {
      if (response[0].msg === "You are now logged in") {
        setTimeout(() => setIsLogged(true), 1500);
      }
    }
  }, [response]);

  return (
    <>
      {isLogged ? (
        <Redirect to={`/${response[0].user}/contactlist`} />
      ) : (
        <Container className="NewUser " style={{ maxWidth: 400 }}>
          <Badge variant="secondary">Login</Badge>
          {response &&
            response.map((response) => {
              return (
                <h6
                  className={
                    response.msg === "You are now logged in"
                      ? "alert-msg success"
                      : "alert-msg danger"
                  }
                  key={response.msg}
                >
                  {response.msg}
                </h6>
              );
            })}
          <Form onSubmit={handleSubmit}>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                value={loginInfo.email}
                name="email"
                onChange={handleChange}
                type="text"
                placeholder="Email"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control
                value={loginInfo.password}
                type="password"
                name="password"
                placeholder="Password"
                onChange={handleChange}
              />
            </Form.Group>

            <ButtonGroup
              style={{
                display: "flex",
                justifyContent: "right",
                paddingBottom: 10,
              }}
            >
              <Col>
                <Button type="submit" variant="secondary">
                  Login{" "}
                </Button>
              </Col>
            </ButtonGroup>
          </Form>
        </Container>
      )}
    </>
  );
};

export default Login;
