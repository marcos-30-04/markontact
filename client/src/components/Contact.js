import React from "react";
import { Col, ButtonGroup, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
const Contact = (props) => {
  const { name, telephone, email, _id, favorite } = props.contact;
  return (
    <div>
      <Col className={favorite ? "Contact favorite" : "Contact"}>
        <br />
        <article className="contact-fields">
          <h3> Name: {name}</h3>
          <h3> Telephone: {telephone}</h3>
          <h3>
            Email:{" "}
            <a href={"mailto:" + email} style={{ color: "rgb(255, 255, 231)" }}>
              {email}
            </a>
          </h3>
        </article>
        <ButtonGroup
          style={{
            display: "flex",
            justifyContent: "space-around",
            padding: 10,
          }}
        >
          <Link to={`/contact/${_id}`}>
            <Button variant="success">View Contact</Button>
          </Link>
          <Button
            onClick={() => props.deleteContact(_id)}
            variant="danger"
            style={{
              maxWidth: 40,
              borderRadius: "50%",
              float: "right",
            }}
          >
            X
          </Button>
        </ButtonGroup>
      </Col>
    </div>
  );
};

export default Contact;
