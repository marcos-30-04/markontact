import React from "react";
import { Navbar, Nav, NavDropdown, Container } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./../App.css";
import axios from "axios";
import { useGlobalContext } from "../context";

function Navigationbar() {
  const { user } = useGlobalContext();
  const logout = () => {
    axios
      .get("https://markontact.herokuapp.com/users/logout")
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };

  return (
    <div className="Navbar">
      <Navbar collapseOnSelect expand="sm" bg="info" variant="dark" fixed="top">
        <Container fluid="lg">
          <Navbar.Brand href="/">Markontact</Navbar.Brand>
          {user && <Navbar.Brand>{`${user}`}</Navbar.Brand>}
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              {user && (
                <>
                  <Nav.Link href={`/${user}/contactlist`}>Contacts</Nav.Link>
                  <Nav.Link href="/create">Add contact</Nav.Link>
                </>
              )}

              {user ? (
                <NavDropdown title="Logout" id="basic-nav-dropdown">
                  <NavDropdown.Item
                    onClick={() => {
                      logout();
                    }}
                  >
                    Logout
                  </NavDropdown.Item>
                </NavDropdown>
              ) : (
                <NavDropdown title="Login/Register" id="basic-nav-dropdown">
                  <NavDropdown.Item href="/login">Login</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="/register">Register</NavDropdown.Item>
                </NavDropdown>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
export default Navigationbar;
