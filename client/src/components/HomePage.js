import React from "react";
import { Spinner } from "react-bootstrap";
const HomePage = () => {
  return (
    <div>
      <header
        style={{ color: "white", fontSize: "40px", fontFamily: "sans-serif" }}
      >
        Welcome To Markontact
      </header>
      <Spinner animation="border" variant="light" />
    </div>
  );
};

export default HomePage;
