import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Navigationbar from "./components/NavBar";
import NewContact from "./components/NewContact.js";
import ContactList from "./components/ContactList.js";
import FullContact from "./components/FullContact.js";
import HomePage from "./components/HomePage";
import Login from "./components/Login.js";
import Register from "./components/Register.js";

import { useGlobalContext } from "./context";

function App() {
  const { user, loading } = useGlobalContext();

  return (
    <>
      {loading ? (
        <div
          className="loading"
          style={{ color: "white", margin: 100, fontSize: "50px" }}
        >
          <h1>Loading...</h1>
        </div>
      ) : (
        <Router>
          <div className="App">
            <nav>
              <Navigationbar user={user} />
            </nav>
            <br />
            <Route path="/" exact component={HomePage} />

            <Route
              path="/:user/contactlist"
              render={(props) => <ContactList {...props} user={user} />}
            />
            <Route path="/contact/:id" component={FullContact} />

            <Route
              path="/create"
              render={(props) => <NewContact {...props} user={user} />}
            />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
          </div>
        </Router>
      )}
    </>
  );
}

export default App;
