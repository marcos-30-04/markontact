import React, { useState, useContext, useEffect } from "react";
import axios from "axios";

axios.defaults.withCredentials = true;

const AppContext = React.createContext();

const AppProvider = ({ children }) => {
  const [user, setUser] = useState();
  const [isUserLoggedIn, setIsUserLoggedIn] = useState(false);
  const [loading, setLoading] = useState(true);
  const [fullContact, setFullContact] = useState({
    address: "",
    email: [],
    favorite: false,
    name: "",
    telephone: [],
    birthday: "",
  });

  const loginUser = () => {
    setIsUserLoggedIn(true);
  };

  const logoutUser = () => {
    console.log("trying to logout");
    axios
      .get("https://markontact.herokuapp.com/users/logout")
      .then((res) => console.log(res))
      .catch((err) => console.log(err));

    setIsUserLoggedIn(false);
  };

  const getFullContact = (id) => {
    setLoading(true);
    axios
      .get(`https://markontact.herokuapp.com/users/${user}/contactlist/${id}`)
      .then((contact) => {
        const {
          address,
          email,
          favorite,
          name,
          telephone,
          birthday,
        } = contact.data;
        setFullContact({
          address,
          email,
          favorite,
          name,
          telephone,
          birthday,
        });
      })
      .finally(setLoading(false))
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    function getUser() {
      setLoading(true);
      axios
        .get("https://markontact.herokuapp.com/users/user")
        .then((res) => {
          setUser(res.data.username);
        })
        .finally(setLoading(false))
        .catch((err) => console.log(err));
    }
    getUser();
  }, []);

  return (
    <AppContext.Provider
      value={{
        user,
        isUserLoggedIn,
        loginUser,
        logoutUser,
        loading,
        setLoading,
        fullContact,
        getFullContact,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useGlobalContext = () => {
  return useContext(AppContext);
};

export { AppContext, AppProvider };
